module RIO.Local
  ( module RIO
  , module RIO.Local
  , module RE
  ) where

import RIO

import GHC.Float as RE (double2Float, float2Double)

import RIO.State (gets)
import Engine.Types (StageFrameRIO)

τ :: Float
τ = 2 * pi

{-# INLINE stageFrameGetRS #-}
stageFrameGetRS :: (rs -> a) -> StageFrameRIO sp p fr rs a
stageFrameGetRS = mapRIO fst . gets
