module Geometry.Intersect
  ( Hit(..)

  , Ray(..)
  , rayFromSegment

  , Plane(..)
  , xz

  , ray'plane
  ) where

import RIO

import Geomancy (Vec3, vec3)
import Geomancy.Vec3 (dot, (^*))

data Ray = Ray
  { rayOrigin    :: Vec3
  , rayDirection :: Vec3
  }
  deriving (Show)

rayFromSegment :: Vec3 -> Vec3 -> Ray
rayFromSegment origin target = Ray
  { rayOrigin    = origin
  , rayDirection = target - origin
  }

data Plane = Plane
  { planeNormal :: Vec3
  , planeDepth  :: Float
  }
  deriving (Show)

-- | XZ plane facing up.
xz :: Float -> Plane
xz d = Plane
  { planeNormal = vec3 0 (-1) 0
  , planeDepth  = d
  }

-- * Intersections

data Hit = Hit
  { hitDistance :: Float
  , hitCosine   :: Float
  , hitPosition :: Vec3
  }
  deriving (Eq, Ord, Show)

ray'plane
  :: Ray
  -> Plane
  -> Maybe Hit
ray'plane Ray{..} Plane{..} =
  -- XXX: Ignore "glancing" hits from either side of a plane
  if (abs denom > eps) then
    Just Hit
      { hitDistance = t
      , hitCosine   = denom
      , hitPosition = hit
      }
  else
    Nothing
  where
    denom = planeNormal `dot` rayDirection
    eps = 1/32

    t = negate ((planeNormal `dot` rayOrigin) + planeDepth) / denom

    hit = rayOrigin + rayDirection ^* t
