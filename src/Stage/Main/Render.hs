{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer

import Global.Render (ScreenPasses(..), Pipelines(..))
import Stage.Main.Render.Scene (prepareCasters, prepareScene)
import Stage.Main.Render.UI (imguiDrawData)
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.World.Cursor3d qualified as Cursor3d
import Stage.Main.World.DebugLines qualified as DebugLines
import Stage.Main.World.GridLines qualified as GridLines
import Stage.Main.World.Object.Selected qualified as ObjectSelected
import Stage.Main.World.Tiles qualified as Tiles

updateBuffers
  :: RunState
  -> FrameResources
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Worker.observeIO_ rsSceneP frScene \_old new -> do
    -- XXX: must stay the same or descsets must be updated with a new buffer
    _same <- Buffer.updateCoherent [new] frSceneData
    pure new

  Worker.observeIO_ rsSceneUiP frSceneUi \_old new -> do
    -- XXX: must stay the same or descsets must be updated with a new buffer
    _same <- Buffer.updateCoherent [new] frSceneUiData
    pure new

  ObjectSelected.observe rsSelectedObject frSelectedObject
  traverse_ (uncurry GridLines.observe) $ zip (toList rsGridLines) (toList frGridLines)
  DebugLines.observe rsDebugLines frDebugLines

  Cursor3d.observe rsCursor3d frCursor3d
  GridLines.observe rsCursorGrid frCursorGrid

  traverse_ (uncurry Tiles.observe) $ zip (toList rsTiles) (toList frTiles)

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
recordCommands cb fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  sceneDrawCasters <- prepareCasters fr
  (sceneDrawOpaque, sceneDrawBlended) <- prepareScene fPipelines fr
  dear <- imguiDrawData

  updateShadow <- stageFrameGetRS rsUpdateShadow
  Worker.observeIO_ updateShadow frUpdateShadow \() () -> do
    Worker.Versioned{vVersion} <- readTVarIO (Worker.getInput updateShadow)
    logDebug $ "Updating shadowmap for version " <> displayShow vVersion
    let shadowLayout = Pipeline.pLayout $ pShadowCast fPipelines
    ShadowPass.usePass (spShadowPass fRenderpass) imageIndex cb do
      let shadowArea = ShadowPass.smRenderArea $ spShadowPass fRenderpass
      Swapchain.setDynamic cb shadowArea shadowArea
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout frSunDescs $
        Pipeline.bind cb (pShadowCast fPipelines) $
          sceneDrawCasters cb

  ForwardMsaa.usePass (spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    let dsl = Pipeline.pLayout $ pWireframe fPipelines
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl frSceneDescs do
      sceneDrawOpaque cb
      sceneDrawBlended cb

    ImGui.draw dear cb
