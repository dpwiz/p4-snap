{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  , Options(..)
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Data.List qualified as List
import Geomancy (vec2, pattern WithVec2)
-- import Geomancy.Transform qualified as Transform
import RIO.App (appEnv)
import RIO.Directory (doesDirectoryExist, getDirectoryContents)
import RIO.FilePath ((</>))
import RIO.State (gets, modify')
import RIO.Vector qualified as Vector
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera (projectionScreen)
import Engine.StageSwitch (getNextStage, newStageSwitchVar)
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (createSet0Ds, createSet0Ds_)
import Render.DescSets.Sun qualified as Sun
import Render.ImGui qualified as ImGui
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer as Buffer
import Resource.CommandBuffer as CommandBuffer
import Resource.Image qualified as Image

import Global.Render qualified
import Global.Resource.Object qualified as Object
import Global.Resource.Static qualified as Static
import Stage.Main.Event.Drag qualified as Drag
import Stage.Main.Events qualified as Events
import Stage.Main.Render qualified as Render
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World.Camera qualified as Camera
import Stage.Main.World.Cursor3d qualified as Cursor3d
import Stage.Main.World.DebugLines qualified as DebugLines
import Stage.Main.World.GridLines qualified as GridLines
import Stage.Main.World.Object.Selected qualified as ObjectSelected
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Tiles qualified as Tiles

data Options = Options
  { resourcePath :: FilePath
  }
  deriving (Show, Generic)

stackStage :: Options -> StackStage
stackStage = StackStage . stage

stage :: Options -> Stage
stage options = Engine.Stage
  { sTitle = "Main"

  , sAllocateP  = Global.Render.allocatePipelines -- TODO: move to class
  , sInitialRS  = initialRunState options
  , sInitialRR  = initialFrameResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands
  , sGetNextStage   = getNextStage rsNextStage

  , sAfterLoop = afterLoop
  }
  where
    beforeLoop = do
      (key, sink) <- Events.spawn
      modify' \ rs -> rs
        { rsEvents = Just sink
        }

      ImGui.beforeLoop True

      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

initialRunState :: Options -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState Options{..} = do
  rsNextStage <- newStageSwitchVar

  context <- ask
  (poolsKey, pools) <- CommandBuffer.allocatePools context

  rsProjectionP <- asks $ Engine.ghScreenP . appEnv
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\input (WithVec2 windowX windowY) ->
        let
          Vk.Extent2D{width, height} = projectionScreen input
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput rsProjectionP)
      rsCursorPos

  rsDragState <- Drag.newState

  (viewKey, rsViewP) <- Worker.registered
    Camera.spawn

  rsCameraControls <- Camera.spawnControls rsViewP
  cameraControlsKey <- Worker.registerCollection rsCameraControls

  rsSceneV <- Worker.newVar Scene.initialInput
  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkScene rsProjectionP rsViewP rsSceneV
  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkSceneUi rsProjectionP rsViewP rsSceneV

  (cubeKey, rsUnitCube) <- Static.cubeWireframe context pools

  rsUpdateShadow <- Worker.newVar ()

  resourceDirs <- getDirectoryContents resourcePath
  modelPacks <- fmap catMaybes $ for resourceDirs \name -> do
    let fullPath = resourcePath </> name
    isDir <- doesDirectoryExist fullPath
    if not isDir || ("." `List.isPrefixOf` name) then
      pure Nothing
    else
      pure $ Just (name, fullPath)
  let (packNames, packPaths) = List.unzip modelPacks

  loadedModels <- traverse Object.loadModels packPaths

  let (modelKeys, modelFiles, models) = List.unzip3 loadedModels
  modelKey <- Resource.register $ traverse_ Resource.release modelKeys
  let
    rsObjectPacks = Vector.fromList packNames
    rsObjectFiles = Vector.fromList modelFiles
    rsObjectCollections = Vector.fromList models

  -- (screenKey, rsScreenBoxP) <- Worker.registered Layout.trackScreen
  let assets = ()
  (uiKey, rsUI) <- UI.spawn assets rsViewP

  (selectedObjectKey, rsSelectedObject) <- Worker.registered $
    ObjectSelected.spawn
      rsObjectCollections
      (UI.selectedObject rsUI)
      (UI.selectedOriginX rsUI)
      (UI.selectedOriginY rsUI)
      (UI.selectedOriginZ rsUI)

  (gridLinesKey, rsGridLines) <- Worker.registeredCollection GridLines.spawn $
    Vector.fromList
      [
      ]
    -- XXX: useful for sub-tile positioning and aligning, when attached to cursor
    -- [ GridLines.Input
    --     { rows = 4
    --     , cols = 4
    --     , color = vec4 1 0.5 0 0.5
    --     , transform =
    --         Transform.rotateX (τ/4)
    --     }
    -- , GridLines.Input
    --     { rows = 4
    --     , cols = 4
    --     , color = vec4 0.25 1 0.25 0.5
    --     , transform =
    --         Transform.rotateY (τ/4)
    --     }
    -- , GridLines.Input
    --     { rows = 4
    --     , cols = 4
    --     , color = vec4 0 0.5 1 0.5
    --     , transform =
    --         Transform.rotateZ (τ/4)
    --     }
    -- ]

  rsTiles <- for rsObjectCollections \collection ->
    Tiles.spawnGroup $
      Vector.replicate (Vector.length collection)
        Tiles.Input
          { groupTiles = mempty
          , tileHeight = 0.5
          }
  tileKeys <- traverse Worker.registerCollection rsTiles
  tilesKey <- Resource.register $ traverse_ Resource.release tileKeys

  (debugLinesKey, rsDebugLines) <- Worker.registered $
    DebugLines.spawn []
      -- [ DebugLines.Edge
      --     { edgeFrom = DebugLines.Point 0 (vec4 1 0 0 1)
      --     , edgeTo   = DebugLines.Point (vec3 1 0 0) (vec4 1 0 0 1)
      --     }
      -- ]

  (cursor3dKey, rsCursor3d) <- Worker.registered $
    Cursor3d.spawn (Worker.getInput rsProjectionP) rsSceneP rsCursorP (UI.currentLevel rsUI)

  (cursorGridKey, rsCursorGrid) <- Worker.registered $
    Cursor3d.spawnGrid rsCursor3d

  releaseKeys <- Resource.register $ traverse_ @[] Resource.release
    [ cursorKey
    -- , debugKey
    , viewKey
    , cameraControlsKey

    , sceneKey, sceneUiKey
    -- , sunKey
    -- , envKey
    -- , screenKey
    , cubeKey
    , modelKey
    -- , terrainKey
    -- , placesKey
    , uiKey
    , selectedObjectKey
    , gridLinesKey
    , tilesKey
    -- , objectsKey
    , debugLinesKey
    , cursor3dKey
    , cursorGridKey
    ]

  Resource.release poolsKey

  let rsEvents = Nothing
  pure (releaseKeys, RunState{..})

initialFrameResources
  :: Queues Vk.CommandPool
  -> Global.Render.ScreenPasses
  -> Global.Render.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources _pools passes pipelines = do
  context <- ask

  let (_sunBB, lights) = Scene.staticLights

  (_lightsKey, lightsData) <- Buffer.allocateCoherent
    context
    Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
    0
    lights

  (frSceneDescs, frSceneData, frScene) <- createSet0Ds
    (Global.Render.getSceneLayout pipelines)
    Nothing
    Nothing
    (Just lightsData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Global.Render.spShadowPass passes
    ]
    Nothing -- TODO: collect materials

  (frSceneUiDescs, frSceneUiData, frSceneUi) <- createSet0Ds_
    (Global.Render.getSceneLayout pipelines)

  (frSunDescs, frSunData) <- Sun.createSet0Ds (Global.Render.getSunLayout pipelines)
  Buffer.updateCoherent lights frSunData

  frSelectedObject <- ObjectSelected.newObserver

  gridlines <- gets rsGridLines
  frGridLines <- for gridlines \_process ->
    GridLines.newObserver

  frTiles <- gets rsTiles >>= traverse (Tiles.newObserver 128)

  frDebugLines <- DebugLines.newObserver 100

  frCursor3d <- Cursor3d.newObserver
  frCursorGrid <- GridLines.newObserver

  frUpdateShadow <- Worker.newObserverIO ()

  pure FrameResources{..}
