module Stage.Main.Event.Types
  ( Event(..)
  , TileCoords
  , TileModelIx
  ) where

import RIO

import Geomancy (Vec2, Vec3)

import Stage.Main.World.Tiles qualified as Tiles

data Event
  = DoNothing
  | PutLine Vec3 Vec3
  | WipeLines
  | TilePlace TileCoords TileModelIx Tiles.InputCell
  | TileRemove TileCoords TileModelIx
  | ChangeLevel Int
  | CameraPan Vec2
  | CameraPanHorizontal Float
  | CameraPanVertical Float
  | CameraTurnAzimuth Float
  | CameraTurnInclination Float
  | CameraZoom Float
  | SceneStore FilePath
  | SceneLoad FilePath
  deriving (Show)

type TileCoords = (Int, Int, Int)

type TileModelIx = (Int, Int)
