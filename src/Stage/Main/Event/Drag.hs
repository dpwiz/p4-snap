module Stage.Main.Event.Drag where

import RIO

import Geomancy (Vec2, vec2)
import GHC.Float (double2Float)
import UnliftIO.Resource (ReleaseKey)

import Engine.Types (StageRIO)
import Engine.Window.CursorPos qualified as CursorPos
import Engine.Worker qualified as Worker

import Engine.Events (Sink(..))

type DragState e = TMVar (Vec2, Vec2 -> Vec2 -> e)

newState :: MonadIO m => m (DragState e)
newState = newEmptyTMVarIO

callback
  :: ( Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e
  -> cursor
  -------------------------
  -> Sink e rs
  -> StageRIO rs ReleaseKey
callback dragState cursorVar =
  CursorPos.callback . handler dragState cursorVar

handler
  :: ( Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e
  -> cursor
  -> Sink e rs
  -> CursorPos.Callback rs
handler dragState cursorVar (Sink signal) windowX windowY = do
  -- logDebug $ "CursorPos event: " <> displayShow cursorPos
  dragIO <- atomically do
    Worker.pushInputSTM cursorVar \_old ->
      cursorPos
    tryTakeTMVar dragState >>= \case
      Nothing ->
        pure Nothing
      Just (dragStart, dragHandler) -> do
        putTMVar dragState (cursorPos, dragHandler)
        pure $ Just do
          logDebug $ "Drag.handler: " <> displayShow (dragStart, cursorPos)
          signal $ dragHandler dragStart cursorPos
  sequence_ dragIO
  where
    cursorPos = vec2 (double2Float windowX) (double2Float windowY)

start
  :: ( Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e -> cursor -> (Vec2 -> Vec2 -> e) -> StageRIO rs Bool
start dragState cursor dragHandler = do
  -- logDebug $ "Drag.start: " <> displayShow cursorPos
  atomically do
    cursorPos <- Worker.getInputDataSTM cursor
    tryPutTMVar dragState (cursorPos, dragHandler)

stop
  :: ( Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e -> cursor -> StageRIO rs ()
stop dragState _cursor =
  atomically $
    tryTakeTMVar dragState >>= \case
      _ ->
        pure () -- XXX: consider stop handler
