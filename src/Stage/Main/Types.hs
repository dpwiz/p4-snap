module Stage.Main.Types
  ( Stage
  , Frame

  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Geomancy (Vec2)
import Vulkan.Core10 qualified as Vk

import Engine.Events qualified as Events
import Engine.StageSwitch (StageSwitchVar)
import Engine.Worker (ObserverIO)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Rendering
import Global.Resource.Object qualified as Object
import Global.Resource.Static qualified as Static
import Stage.Main.UI (UI)
import Stage.Main.Event.Types (Event)
import Stage.Main.World.Camera qualified as Camera
import Stage.Main.World.Cursor3d qualified as Cursor3d
import Stage.Main.World.DebugLines qualified as DebugLines
import Stage.Main.World.GridLines qualified as GridLines
import Stage.Main.World.Object.Selected qualified as ObjectSelected
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Tiles qualified as Tiles
import Stage.Main.Event.Drag (DragState) -- TODO: extract to engine

type Stage = Rendering.Stage FrameResources RunState

type Frame = Rendering.Frame FrameResources

data FrameResources = FrameResources
  { frSceneDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frScene      :: ObserverIO Scene

  , frSceneUiDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneUiData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frSceneUi      :: ObserverIO Scene

  , frSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , frSunData  :: Buffer.Allocated 'Buffer.Coherent Sun

  , frUpdateShadow :: ObserverIO ()

  , frSelectedObject :: ObjectSelected.Observer
  , frGridLines      :: Vector GridLines.Observer

  , frTiles :: Vector Tiles.ObserverGroup

  , frDebugLines :: DebugLines.Observer

  , frCursor3d :: Cursor3d.Observer
  , frCursorGrid :: GridLines.Observer
  }

data RunState = RunState
  { rsNextStage   :: StageSwitchVar
  , rsEvents      :: Maybe (Events.Sink Event RunState)

  , rsProjectionP :: Camera.ProjectionProcess
  , rsViewP       :: Camera.ViewProcess

  , rsCameraControls :: Camera.ControlsProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsDragState :: DragState Event

  , rsSceneP :: Scene.Process
  , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  , rsObjectPacks       :: Vector FilePath
  , rsObjectFiles       :: Vector Object.Files
  , rsObjectCollections :: Vector Object.Collection

  , rsUI :: UI

  , rsUnitCube :: Static.UnlitColored

  , rsUpdateShadow :: Worker.Var ()

  , rsSelectedObject :: ObjectSelected.Process
  , rsGridLines      :: Vector GridLines.Process

  , rsTiles :: Vector Tiles.ProcessGroup

  , rsDebugLines :: DebugLines.Process

  , rsCursor3d :: Cursor3d.Process
  , rsCursorGrid :: Worker.Merge GridLines.Output
  }
