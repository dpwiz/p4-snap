module Stage.Main.World.GridLines
  ( Input(..)
  , emptyInput

  , Process
  , Output
  , spawn
  , mkGridLines

  , Observer
  , newObserver
  , observe
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT, register)
import Geomancy (Transform, Vec4, vec3)
import Geomancy.Transform qualified as Transform
import Geomancy.Vec4 ((^*))
import Geomancy.Vec3 qualified as Vec3
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Render.Unlit.Colored.Model qualified as UnlitColored

data Input = Input
  { rows      :: Word32
  , cols      :: Word32
  , color     :: Vec4
  , transform :: Transform -- XXX: use a plane?
  }
  deriving (Show)

emptyInput :: Input
emptyInput = Input
  { rows      = 0
  , cols      = 0
  , color     = 0
  , transform = mempty
  }

type Process = Worker.Cell Input Output
type Output = (Vertices, Instances)

type Vertices = [Model.Vertex Vec3.Packed UnlitColored.VertexAttrs]
type Instances = Storable.Vector Transform

spawn :: MonadUnliftIO m => Input -> m Process
spawn = Worker.spawnCell mkGridLines

-- TODO: extract instances to a Merge
mkGridLines :: Input -> Output
mkGridLines Input{..} =
  ( horizontals <> verticals
  , Storable.singleton $ mconcat
      [ Transform.translate (-0.5) (-0.5) 0
      , transform
      ]
  )
  where
    horizontals = mconcat do
      y <- [0 .. fRows]
      let
        y' = y / fRows
        fade = (0.75 - abs (y' - 0.5))
        faded = color ^* fade
      pure
        [ Model.Vertex
            { vPosition = Vec3.Packed $ vec3 0 y' 0
            , vAttrs    = faded
            }
        , Model.Vertex
            { vPosition = Vec3.Packed $ vec3 1 y' 0
            , vAttrs    = faded
            }
        ]

    verticals = mconcat do
      x <- [0 .. fCols]
      let
        x' = x / fCols
        fade = (0.75 - abs (x' - 0.5))
        faded = color ^* fade
      pure
        [ Model.Vertex
            { vPosition = Vec3.Packed $ vec3 x' 0 0
            , vAttrs    = faded
            }
        , Model.Vertex
            { vPosition = Vec3.Packed $ vec3 x' 1 0
            , vAttrs    = faded
            }
        ]

    fRows = fromIntegral rows
    fCols = fromIntegral cols

type Observer = Worker.ObserverIO
  ( UnlitColored.Model 'Buffer.Coherent
  , Buffer.Allocated 'Buffer.Coherent UnlitColored.InstanceAttrs
  )

newObserver :: ResourceT (StageRIO st) Observer
newObserver = do
  context <- ask

  model <- Model.createCoherentEmpty context (2 * 2 * 100)
  _modelKey <- register $ Model.destroyIndexed context model

  (_instanceKey, instances) <- Buffer.allocateCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    mempty

  Worker.newObserverIO (model, instances)

observe
  :: ( HasVulkan env
     , Worker.HasOutput process
     , Worker.GetOutput process ~ Output
     )
  => process
  -> Observer
  -> RIO env ()
observe process observer = do
  context <- ask
  Worker.observeIO_ process observer \(oldModel, oldInstance) (newVertices, newInstance) ->
    (,)
      <$> Model.updateCoherent context newVertices oldModel
      <*> Buffer.updateCoherent newInstance oldInstance
