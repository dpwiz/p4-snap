module Stage.Main.World.Camera
  ( Camera.ProjectionProcess
  , Camera.ViewProcess
  , spawn

  , Controls(..)
  , ControlsProcess
  , spawnControls

  , panInstant
  ) where

import RIO.Local

import Geomancy (Vec2, Vec3, vec3, withVec2)
import Geomancy.Quaternion qualified as Quaternion

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker

spawn :: RIO env Camera.ViewProcess
spawn = do
  Worker.spawnCell Camera.mkViewOrbital_ initialViewInput

initialViewInput :: Camera.ViewOrbitalInput
initialViewInput = Camera.ViewOrbitalInput
  { orbitAzimuth  = 0 -- τ/8
  , orbitAscent   = τ/7
  , orbitDistance = 8.0
  , orbitScale    = 1
  , orbitTarget   = 0
  }

data Controls a = Controls
  { panHorizontal   :: a
  , panVertical     :: a
  , turnAzimuth     :: a
  , turnInclination :: a
  }
  deriving (Functor, Foldable, Traversable)

type ControlsProcess = Controls (Worker.Timed Float ())

spawnControls :: Camera.ViewProcess -> RIO env ControlsProcess
spawnControls vp =
  traverse mkUpdater Controls
    { panHorizontal   = panTargetHorizontal
    , panVertical     = panTargetVertical
    , turnAzimuth     = orbitAzimuthTurn
    , turnInclination = orbitAscentTurn
    }
  where
    vpInput = Worker.getInput vp

    dtI = 1e3
    dt = fromIntegral dtI / 1e6

    mkUpdater updater =
      Worker.spawnTimed
        (\acceleration delta -> do
            when (delta /= 0) $
              Worker.pushInput vpInput $
                updater (1 + acceleration) (delta * dt)
            pure
              ( ()
              , if delta == 0 then
                  if acceleration < 1/128 then
                    acceleration
                  else
                    acceleration * 0.97
                else
                  acceleration + 0.01
              )
        )
        (Left dtI)
        True
        0
        0

    panTargetHorizontal acceleration delta voi@Camera.ViewOrbitalInput{orbitAzimuth} = voi
      { Camera.orbitTarget = Camera.orbitTarget voi + pan
      }
      where
        pan = Quaternion.rotate
          (Quaternion.axisAngle axisUp orbitAzimuth)
          (vec3 (delta * acceleration) 0 0)

    panTargetVertical acceleration delta voi@Camera.ViewOrbitalInput{orbitAzimuth} = voi
      { Camera.orbitTarget = Camera.orbitTarget voi + pan
      }
      where
        pan = Quaternion.rotate
          (Quaternion.axisAngle axisUp orbitAzimuth)
          (vec3 0 0 (delta * acceleration))

    orbitAzimuthTurn _acceleration delta voi@Camera.ViewOrbitalInput{orbitAzimuth} = voi
      { Camera.orbitAzimuth = azimuth
      }
      where
        azimuth
          | azimuth' < (-τ) = azimuth' + (2 * τ)
          | azimuth' > τ = azimuth' - (2 * τ)
          | otherwise = azimuth'

        azimuth' = orbitAzimuth + delta

    orbitAscentTurn _acceleration delta voi@Camera.ViewOrbitalInput{orbitAscent} = voi
      { Camera.orbitAscent = ascent
      }
      where
        ascent = max (-limit) . min limit $ orbitAscent + delta

        limit = τ/4 - 1/512

axisUp :: Vec3
axisUp = vec3 0 (-1) 0

panInstant :: MonadIO m => Camera.ViewProcess -> Vec2 -> m ()
panInstant vp delta = do
  -- traceShowM delta
  Worker.pushInput vp \voi@Camera.ViewOrbitalInput{orbitAzimuth} -> voi
    { Camera.orbitTarget =
        -- traceShow (pan orbitAzimuth) $
          Camera.orbitTarget voi + pan orbitAzimuth
    }
  where
    -- vpInput = Worker.getInput vp

    pan azimuth = Quaternion.rotate
      (Quaternion.axisAngle axisUp azimuth)
      delta3 * 0.01

    delta3 =
      withVec2 delta \x z ->
        vec3 x 0 (-z)
