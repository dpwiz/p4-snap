module Stage.Main.World.Scene where

import RIO.Local

import Geomancy (Transform, Vec4, vec3, vec4)
import Geomancy.Transform qualified as Transform
import Geomancy.Vec4 qualified as Vec4
import RIO.Vector.Storable qualified as Storable

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..))
import Render.DescSets.Sun (Sun(..))

type InputVar = Worker.Var Input

data Input = Input

initialInput :: Input
initialInput = Input

type Process = Worker.Merge Scene

mkScene :: Camera.Projection -> Camera.View -> Input -> Scene
mkScene Camera.Projection{..} Camera.View{..} Input = Scene{..}
  where
    sceneProjection    = projectionPerspective
    sceneInvProjection = Transform.inverse projectionPerspective -- FIXME: move to cell output

    sceneView          = viewTransform
    sceneInvView       = viewTransformInv
    sceneViewPos       = viewPosition
    sceneViewDir       = viewDirection

    sceneFog           = 0
    sceneEnvCube       = -1
    sceneNumLights     = fromIntegral $ Storable.length $ snd staticLights
    sceneTweaks        = 0

mkSceneUi :: Camera.Projection -> Camera.View -> Input -> Scene
mkSceneUi camera@Camera.Projection{..} cameraView input =
  (mkScene camera cameraView input)
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    , sceneView          = mempty
    , sceneInvView       = mempty
    }

staticLights :: (Transform, Storable.Vector Sun)
staticLights = (sunBB, lights)
  where
    (sunBB, theSun) = mkSun (τ/9) (τ/10) 1.5 (Just 0)
    lights =
      Storable.fromList
        [ theSun
        , Sun
            { sunViewProjection = mempty -- XXX: shadows ?
            , sunShadow         = 0
            , sunPosition       = Vec4.fromVec3 (vec3 0 (-10) 0) 0
            , sunDirection      = Vec4.fromVec3 (vec3 0 (-1) 0) 0
            , sunColor          = 0.125
            }
        , Sun
            { sunViewProjection = mempty -- XXX: no shadows
            , sunShadow         = 0
            , sunPosition       = Vec4.fromVec3 (vec3 0 10 0) 0
            , sunDirection      = Vec4.fromVec3 (vec3 0 1 0) 0
            , sunColor          = 0.125
            }
        ]

mkSun :: Float -> Float -> Vec4 -> Maybe Natural -> (Transform, Sun)
mkSun azimuth inclination color mshadow =
  ( bbTransform
  , Sun
      { sunViewProjection = mconcat vp
      , sunShadow         = vec4 0 0 (maybe (-1) fromIntegral mshadow) size
      , sunPosition       = Vec4.fromVec3 position 0 -- Vec4.fromVec3 (vec3 0 (-10) 0) 0
      , sunDirection      = Vec4.fromVec3 direction 0 -- Vec4.fromVec3 (Vec3.normalize $ vec3 0.25 (-0.5) 0.5) 0
      , sunColor          = color
      }
  )
  where
    size = 32
    depthRange = 128
    distance = depthRange / 2

    -- XXX: copypasta from playgroun4:Stage.Example.World.Sun

    vp =
      [ Transform.rotateY (-azimuth)
      , Transform.rotateX (-inclination)

      , Transform.translate 0 0 distance

      -- XXX: some area beyond the near plane receives light, but not shadows
      , Transform.scale3
          (1 / size)
          (1 / size)
          (1 / depthRange)
      ]

    position = Transform.apply (vec3 0 0 distance) rotation

    direction = Transform.apply (vec3 0 0 $ -1) rotation

    rotation = mconcat
      [ Transform.rotateX inclination
      , Transform.rotateY azimuth
      ]

    bbTransform = mconcat
      [ -- XXX: orient wire box "green/near -> far/red"
        Transform.rotateX (τ/4)
        -- XXX: the rest must be matched with VP flipped
      , Transform.translate 0 0 0.5                 -- XXX: shift origin to the near face
      , Transform.scale3 size size depthRange -- XXX: size to projection volume
      , Transform.translate 0 0 (-distance)         -- XXX: translate near face to radius
      , rotation                                    -- XXX: apply sphere coords
      ]
