module Stage.Main.World.Tiles where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (Transform, Vec3, vec3)
import Geomancy.Transform qualified as Transform
import RIO.Map qualified as Map
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Render.Lit.Colored.Model qualified as LitColored
import Resource.Buffer qualified as Buffer

type ProcessGroup = Vector Process

type Process = Worker.Cell Input Output

data Input = Input
  { groupTiles :: Map (Int, Int, Int) InputCell
  , tileHeight :: Float
  }
  deriving (Show, Generic)

data InputCell = InputCell
  { tileScale     :: Float
  , tileDirection :: Direction
  , tileOrigin    :: Vec3
  }
  deriving (Show, Generic)

data Direction
  = North
  | East
  | South
  | West
  deriving (Eq, Ord, Show, Enum, Bounded, Generic)

north :: InputCell
north = InputCell 1.0 North 0

east :: InputCell
east = InputCell 1.0 East 0

south :: InputCell
south = InputCell 1.0 South 0

west :: InputCell
west = InputCell 1.0 West 0

type Output = Storable.Vector LitColored.InstanceAttrs

-- | Convert a collection of models and associated inputs into group of workers.
spawnGroup :: MonadUnliftIO m => Vector Input -> m ProcessGroup
spawnGroup = traverse (Worker.spawnCell mkTiles)

mkTiles :: Input -> Output
mkTiles Input{..} = Storable.fromList do
  ((ew, ns, level), InputCell{..}) <- Map.toList groupTiles

  let
    cellOrigin = vec3
      (fromIntegral ew)
      (fromIntegral level * tileHeight)
      (fromIntegral ns)

  pure $
    mconcat
      [ Transform.translateV tileOrigin
      , directionRotateY tileDirection
      , Transform.scale tileScale
      , Transform.translateV cellOrigin
      ]

{-# INLINE directionRotateY #-}
directionRotateY :: Direction -> Transform
directionRotateY dir =
  Transform.rotateY $ fromIntegral (fromEnum dir) * λ
  where
    λ = τ / 4

type ObserverGroup = Vector (Worker.ObserverIO Buffer)

type Buffer = Buffer.Allocated 'Buffer.Coherent LitColored.InstanceAttrs

newObserver :: Int -> ProcessGroup -> ResourceT (Engine.StageRIO st) ObserverGroup
newObserver initialSize cells = do
  context <- ask

  buffers <- for cells \_model ->
    Buffer.createCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT initialSize mempty

  observers <- traverse Worker.newObserverIO buffers

  void $! Resource.register $
    -- XXX: the buffers may be resized so we have to read the current batch
    for_ observers \o -> do
      buffer <- Worker.readObservedIO o
      Buffer.destroy context buffer

  pure observers

observe :: HasVulkan env => ProcessGroup -> ObserverGroup -> RIO env ()
observe cells observers = do
  context <- ask
  for_ (zip (toList cells) (toList observers)) \(process, observer) ->
    Worker.observeIO_ process observer $
      Buffer.updateCoherentResize_ context
