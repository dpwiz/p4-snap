module Stage.Main.World.Cursor3d
  ( Process
  , spawn
  , mkCursor3d

  , spawnGrid
  , mkCursorGrid

  , Observer
  , newObserver
  , observe
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (Vec2, vec2, vec3, withVec2, withVec3)
import Geomancy.Transform qualified as Transform
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Render.Lit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Render.DescSets.Set0 (Scene(..))

import Geometry.Intersect (Hit(..), xz, rayFromSegment, ray'plane)
import Stage.Main.World.GridLines qualified as GridLines

type Process = Worker.Merge Attrs

type Attrs =
  ( Maybe (Int, Int, Int)
  , Storable.Vector UnlitColored.InstanceAttrs
  )
spawn
  :: ( Worker.HasOutput projectionInput
     , Worker.GetOutput projectionInput ~ Camera.ProjectionInput
     , Worker.HasOutput scene
     , Worker.GetOutput scene ~ Scene
     , Worker.HasOutput cursorPos
     , Worker.GetOutput cursorPos ~ Vec2
     , Worker.HasOutput currentLevel
     , Worker.GetOutput currentLevel ~ Int
     )
  => projectionInput
  -> scene
  -> cursorPos
  -> currentLevel
  -> RIO env Process
spawn = Worker.spawnMerge4 mkCursor3d

mkCursor3d
  :: Camera.ProjectionInput
  -> Scene
  -> Vec2
  -> Int
  -> Attrs
mkCursor3d projectionInput scene cursorPos currentLevel =
  case ray'plane (rayFromSegment origin target) plane of
    Nothing ->
      ( Nothing
      , mempty
      )
    Just Hit{hitPosition} ->
      withVec3 hitPosition \x _y z ->
        let
          we = round x
          ns = round z
        in
          ( Just (we, ns, currentLevel)
          , Storable.singleton $
              Transform.translate
                (fromIntegral we)
                y
                (fromIntegral ns)
          )
  where
    tileHeight = 0.5
    y = fromIntegral currentLevel * tileHeight
    plane = xz (y + 0.5)

    Camera.ProjectionInput{projectionScreen} = projectionInput
    Vk.Extent2D{width=iWidth, height=iHeight} = projectionScreen
    halfWidth = fromIntegral iWidth / 2
    halfHeight = fromIntegral iHeight / 2

    (curX, curY) = withVec2 (cursorPos / vec2 halfWidth halfHeight) (,)
    zNear = 1/2048
    zFar = 1 - 1/16384

    Scene{sceneInvProjection, sceneInvView} = scene
    inverse = sceneInvProjection <> sceneInvView
    unproject curZ = Transform.apply (vec3 curX curY curZ) inverse

    origin = unproject zNear
    target = unproject zFar

spawnGrid :: Process -> RIO env (Worker.Merge GridLines.Output)
spawnGrid = Worker.spawnMerge1 (mkCursorGrid . fst)

mkCursorGrid :: Maybe (Int, Int, Int) -> GridLines.Output
mkCursorGrid mcell = GridLines.mkGridLines input
  where
    units = 5

    input = case mcell of
      Nothing ->
        GridLines.emptyInput
      Just (ew, ns, level) ->
        GridLines.Input
          { rows  = units
          , cols  = units
          , color = 0.5
          , transform = mconcat
              [ Transform.scale (fromIntegral units)
              , Transform.rotateX (τ/4)
              , Transform.translate
                  (fromIntegral ew)
                  (fromIntegral level * 0.5 + 0.5 - 1/256)
                  (fromIntegral ns)
              ]
          }

type Buffer = Buffer.Allocated 'Buffer.Coherent UnlitColored.InstanceAttrs

type Observer = Worker.ObserverIO Buffer

newObserver :: ResourceT (Engine.StageRIO st) Observer
newObserver = do
  context <- ask

  instanceData <- Buffer.createCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 mempty
  observer <- Worker.newObserverIO instanceData

  void $! Resource.register do
    buffer <- Worker.readObservedIO observer
    Buffer.destroy context buffer

  pure observer

observe
  :: ( HasVulkan env
     , Worker.HasOutput source
     , Worker.GetOutput source ~ Attrs
     )
  => source
  -> Observer -> RIO env ()
observe process observer = do
  context <- ask
  Worker.observeIO_ process observer \buf (_mpos, attrs) ->
    Buffer.updateCoherentResize_ context buf attrs
