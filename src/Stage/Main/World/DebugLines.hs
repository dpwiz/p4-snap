module Stage.Main.World.DebugLines
  ( Input
  , Point(..)
  , Edge(..)

  , Process
  , spawn
  , mkDebugLines

  , Observer
  , newObserver
  , observe
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT, register)
import Geomancy (Transform, Vec3, Vec4)
import Geomancy.Vec3 qualified as Vec3
import Geometry.Edge (Edge(..))
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Render.Unlit.Colored.Model qualified as UnlitColored

type Input = [Edge Point]

data Point = Point
  { position :: Vec3
  , color    :: Vec4
  }
  deriving (Show)

type Process = Worker.Cell Input (Vertices, Instances)

type Vertices = [Model.Vertex Vec3.Packed UnlitColored.VertexAttrs]

type Instances = Storable.Vector Transform

spawn :: MonadUnliftIO m => Input -> m Process
spawn = Worker.spawnCell mkDebugLines

mkDebugLines :: Input -> (Vertices, Instances)
mkDebugLines edges =
  ( vertices
  , Storable.singleton mempty
  )
  where
    vertices = edges >>= \Edge{..} ->
      [ Model.Vertex
          { vPosition = Vec3.Packed $ position edgeFrom
          , vAttrs    = color edgeFrom
          }
      , Model.Vertex
          { vPosition = Vec3.Packed $ position edgeTo
          , vAttrs    = color edgeTo
          }
      ]

-- XXX: copypasta from gridlines

type Observer = Worker.ObserverIO
  ( UnlitColored.Model 'Buffer.Coherent
  , Buffer.Allocated 'Buffer.Coherent UnlitColored.InstanceAttrs
  )

newObserver :: Int -> ResourceT (StageRIO st) Observer
newObserver edges = do
  context <- ask

  model <- Model.createCoherentEmpty context (2 * edges) -- XXX: divergence from grid, only one set of edges
  _modelKey <- register $ Model.destroyIndexed context model

  (_instanceKey, instances) <- Buffer.allocateCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    mempty

  Worker.newObserverIO (model, instances)

observe :: HasVulkan env => Process -> Observer -> RIO env ()
observe process observer = do
  context <- ask
  Worker.observeIO_ process observer \(oldModel, oldInstance) (newVertices, newInstance) ->
    (,)
      <$> Model.updateCoherent context newVertices oldModel
      <*> Buffer.updateCoherent newInstance oldInstance
