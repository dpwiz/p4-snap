module Stage.Main.World.Object.Selected
  ( Process
  , spawn

  , Observer
  , newObserver
  , observe
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (Vec3, vec3)
import Geomancy.Transform qualified as Transform
import RIO.Vector ((!?))
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Render.Lit.Colored.Model qualified as LitColored
import Resource.Buffer qualified as Buffer
import Resource.Gltf.Weld qualified as Weld
import Resource.Mesh.Types qualified as Mesh

import Global.Resource.Object (Object)
import Global.Resource.Object qualified as Object

type Process = Worker.Merge Output

type Output = (Vec3, Storable.Vector LitColored.InstanceAttrs, Maybe Object)

spawn
  :: ( Worker.HasOutput selected
     , Worker.GetOutput selected ~ (Int, Int)
     , Worker.HasOutput originX
     , Worker.GetOutput originX ~ Maybe Weld.Origin
     , Worker.HasOutput originY
     , Worker.GetOutput originY ~ Maybe Weld.Origin
     , Worker.HasOutput originZ
     , Worker.GetOutput originZ ~ Maybe Weld.Origin
     )
  => Vector (Vector Object)
  -> selected
  -> originX
  -> originY
  -> originZ
  -> RIO env Process
spawn modelSets = Worker.spawnMerge4 (mkOrigin modelSets)

mkOrigin
  :: Vector (Vector Object)
  -> (Int, Int)
  -> Maybe Weld.Origin
  -> Maybe Weld.Origin
  -> Maybe Weld.Origin
  -> Output
mkOrigin modelSets (setIx, modelIx) mox moy moz =
  ( origin
  , attrs
  , mobject
  )
  where
    origin =
      case mobject of
        Nothing ->
          0
        Just (Object.meta -> Mesh.Meta{mMeasurements}) ->
          cellOrigin + Weld.originV mMeasurements (catMaybes [mox, moy, moz])

    cellOrigin = vec3
      (maybe 0 cellOriginX mox)
      (maybe 0 cellOriginY moy)
      (maybe 0 cellOriginZ moz)
      where
        cellOriginX = \case
          Weld.OriginXLeft   -> -0.5
          Weld.OriginXRight  -> 0.5
          _                  -> 0

        cellOriginY = \case
          Weld.OriginYTop    -> -0.5
          Weld.OriginYBottom -> 0.5
          _                  -> 0

        cellOriginZ = \case
          Weld.OriginZNear   -> -0.5
          Weld.OriginZFar    -> 0.5
          _                  -> 0

    attrs = case mobject of
      Nothing ->
        mempty
      Just _object ->
        Storable.singleton $ Transform.translateV origin

    mobject = do
      models <- modelSets !? setIx
      models !? modelIx

type Buffer = Buffer.Allocated 'Buffer.Coherent LitColored.InstanceAttrs

type Observer = Worker.ObserverIO Buffer

newObserver :: ResourceT (Engine.StageRIO st) Observer
newObserver = do
  context <- ask

  instanceData <- Buffer.createCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 mempty
  observer <- Worker.newObserverIO instanceData

  void $! Resource.register do
    buffer <- Worker.readObservedIO observer
    Buffer.destroy context buffer

  pure observer

observe
  :: ( HasVulkan env
     , Worker.HasOutput source
     , Worker.GetOutput source ~ Output
     )
  => source
  -> Observer -> RIO env ()
observe messageP observer = do
  context <- ask
  Worker.observeIO_ messageP observer \buf (_origin, attrs, _mobject) ->
    Buffer.updateCoherentResize_ context buf attrs
