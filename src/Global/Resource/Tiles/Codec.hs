{-# OPTIONS_GHC -Wwarn=orphans #-}

module Global.Resource.Tiles.Codec
  ( store
  , load
  ) where

import RIO

import Codec.Serialise (Serialise(..), readFileDeserialise, writeFileSerialise)
import Geomancy (Vec3, withVec3)
import Geomancy.Vec3 qualified as Vec3
import RIO.FilePath ((</>), (<.>))
import RIO.Map qualified as Map
import RIO.Vector ((!?))
import RIO.Vector qualified as Vector

import Engine.Worker qualified as Worker

import Stage.Main.World.Tiles qualified as Tiles

data PackageV1 = PackageV1
  { v1_objects :: Vector (FilePath, Vector (FilePath, Tiles.Input))
  }
  deriving (Show, Generic)

instance Serialise PackageV1

instance Serialise Tiles.Input
instance Serialise Tiles.InputCell
instance Serialise Tiles.Direction

instance Serialise Vec3 where
  encode v = encode $ withVec3 v (,,)
  decode = fmap Vec3.fromTuple decode

-- TODO: use zstd

store
  :: HasLogFunc env
  => FilePath
  -> Vector Tiles.ProcessGroup
  -> Vector FilePath
  -> Vector (Map FilePath Int)
  -> RIO env ()
store file groups packs files = do
  logInfo $ "Storing scene to " <> fromString file
  v0_tiles <- atomically do
    for groups \group ->
      for group \worker ->
        fmap Worker.vData . readTVar $ Worker.getInput worker
  -- liftIO $ writeFileSerialise file v0_tiles

  let
    v1 = PackageV1 do
      (pack, packMap, tiles) <- Vector.zip3 packs files v0_tiles
      let tilesFull = Vector.zip (Vector.fromList $ Map.keys packMap) tiles

      let
        tilesSparse = do
          (collection, inputs) <- tilesFull
          guard $ not (Map.null $ Tiles.groupTiles inputs)
          pure (collection, inputs)

      guard $ not (Vector.null tilesSparse)
      pure
        ( pack
        , tilesSparse
        )
  liftIO $ writeFileSerialise (file <.> "v1") v1

load
  :: HasLogFunc env
  => FilePath
  -> Vector Tiles.ProcessGroup
  -> Vector FilePath
  -> Vector (Map FilePath Int)
  -> RIO env ()
load file groups packs files = do
  logInfo $ "Loading scene from " <> fromString file
  -- v0_tiles <- liftIO $ readFileDeserialise file
  -- atomically do
  --   for_ (Vector.zip groups v0_tiles) \(group, loadedGroup) ->
  --     for_ (Vector.zip group loadedGroup) \(worker, newInput) ->
  --       Worker.pushInputSTM worker $ const newInput

  let packIds = Map.fromList . toList $ Vector.imap (flip (,)) packs
  PackageV1 v1_objects <- liftIO $ readFileDeserialise (file <.> "v1")

  actions <- atomically do
    -- XXX: wipe the scene first, as the V1 is sparse.
    for_ groups \group ->
      for_ group \worker ->
        Worker.pushInputSTM worker $ const (Tiles.Input mempty 0.5)

    actions <- newTVar mempty
    let
      register action =
        modifyTVar' actions (action :)

    for_ v1_objects \(packPath, packed) ->
      case Map.lookup packPath packIds of
        Nothing ->
          register . logWarn $ mconcat
            [ "Missing object pack: "
            , displayShow packPath
            ]
        Just packId ->
          case Vector.zip groups files !? packId of
            Nothing ->
              register . logWarn $ mconcat
                [ "Missing worker group for "
                , displayShow packPath
                , ": "
                , display packId
                ]
            Just (group, paths) ->
              for_ packed \(objectPath, tiles) ->
                case Map.lookup objectPath paths of
                  Nothing ->
                    register . logWarn $ mconcat
                      [ "Missing object "
                      , displayShow $ packPath </> objectPath
                      ]
                  Just objectId ->
                    case group !? objectId of
                      Nothing ->
                        register . logWarn $ mconcat
                          [ "Missing worker for object "
                          , displayShow $ packPath </> objectPath
                          , ": "
                          , display objectId
                          ]
                      Just process ->
                        Worker.pushInputSTM process $ const tiles
    pure actions

  readTVarIO actions >>= sequence_ . reverse
