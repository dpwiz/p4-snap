{-# LANGUAGE OverloadedLists #-}

module Global.Resource.Static
  ( UnlitColored
  , cubeWireframe
  ) where

import RIO.Local

import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Types (HasVulkan, Queues)
import Geometry.Cube qualified as Cube
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model

type UnlitColored =
  ( UnlitColored.Model 'Buffer.Staged
  , Buffer.Allocated 'Buffer.Staged UnlitColored.InstanceAttrs
  )

cubeWireframe
  :: ( HasVulkan context
     , MonadUnliftIO m
     , Resource.MonadResource m
     )
  => context
  -> Queues Vk.CommandPool
  -> m ( Resource.ReleaseKey
       , UnlitColored
       )
cubeWireframe context pools = do
  model <- Model.createStagedL context pools Cube.bbWireColored Nothing

  attrs <- Buffer.createStaged context pools Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 [mempty]

  key <- Resource.register do
    Model.destroyIndexed context model
    Buffer.destroy context attrs

  pure (key, (model, attrs))
