module Global.Resource.Object
  ( Files
  , Collection
  , Settings(..)
  , settingsMiddle
  , settingsMidBottom
  , Object(..)
  , loadModels
  , storeSettings
  , settingsFile
  ) where

import RIO.Local

import Codec.GlTF (GlTF)
import Data.Tree (Tree)
import Data.Yaml qualified as Yaml
import Geomancy (Transform, Vec3, vec3, withVec3)
import Geomancy.Mat4 qualified as Mat4
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import RIO.Directory (doesFileExist, getDirectoryContents)
import RIO.FilePath (splitExtensions, (</>), (-<.>))
import RIO.List qualified as List
import RIO.Map qualified as Map
import RIO.Vector qualified as Vector
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource qualified as Resource

import Engine.Types (StageRIO)
import Render.Lit.Colored.Model qualified as LitColored
import Resource.Buffer qualified as Buffer
import Resource.Collection qualified as Collection
import Resource.CommandBuffer as CommandBuffer
import Resource.Gltf.Load qualified as Gltf
import Resource.Gltf.Model qualified as Gltf
import Resource.Gltf.Scene qualified as Gltf
import Resource.Gltf.Weld qualified as Weld
import Resource.Mesh.Types qualified as Mesh
import Resource.Model qualified as Model

type Files = Map FilePath Int

type Collection = Vector Object

data Object = Object
  { source   :: FilePath
  , settings :: Settings
  , gltf     :: GlTF
  , scene    :: Tree Gltf.SceneNode
  , meta     :: Mesh.Meta
  , nodes    :: Mesh.Nodes
  , model    :: LitColored.Model 'Buffer.Staged
  }

data Settings = Settings
  { rootTransform  :: Transform
  , measuredOrigin :: (Weld.Origin, Weld.Origin, Weld.Origin)
  , adjustOrigin   :: Vec3
  }
  deriving (Show)

settingsMiddle :: Settings
settingsMiddle = Settings
  { rootTransform =
      Transform.rotateZ (τ/2)
  , measuredOrigin =
      ( Weld.OriginXMiddle
      , Weld.OriginYMiddle
      , Weld.OriginZMiddle
      )
  , adjustOrigin = 0
  }

settingsMidBottom :: Settings
settingsMidBottom = settingsMiddle
  { measuredOrigin =
      ( Weld.OriginXMiddle
      , Weld.OriginYBottom
      , Weld.OriginZMiddle
      )
  , adjustOrigin = vec3 0 0.5 0
  }

instance Yaml.FromJSON Settings where
  parseJSON = Yaml.withObject "Settings" \o -> do
    rootTransform <- fmap (Mat4.fromRowMajor2d @[]) (o Yaml..: "transform") >>= \case
      Nothing ->
        fail "transform must be a 4x4 array"
      Just mat4 ->
        pure mat4

    originX <- (o Yaml..: "origin-x") >>= \case
      "left"   -> pure Weld.OriginXLeft
      "middle" -> pure Weld.OriginXMiddle
      "mean"   -> pure Weld.OriginXMean
      "right"  -> pure Weld.OriginXRight
      ox       -> fail $ "Unexpected origin-x" <> show @Text ox

    originY <- (o Yaml..: "origin-y") >>= \case
      "top"    -> pure Weld.OriginYTop
      "middle" -> pure Weld.OriginYMiddle
      "mean"   -> pure Weld.OriginYMean
      "bottom" -> pure Weld.OriginYBottom
      oy       -> fail $ "Unexpected origin-x" <> show @Text oy

    originZ <- (o Yaml..: "origin-z") >>= \case
      "near"   -> pure Weld.OriginZNear
      "middle" -> pure Weld.OriginZMiddle
      "mean"   -> pure Weld.OriginZMean
      "far"    -> pure Weld.OriginZFar
      oz       -> fail $ "Unexpected origin-x" <> show @Text oz

    let measuredOrigin = (originX, originY, originZ)

    adjustOrigin <- fmap Vec3.fromTuple (o Yaml..: "origin")

    pure Settings{..}

instance Yaml.ToJSON Settings where
  toJSON Settings{..} = Yaml.object
    [ "transform" Yaml..= Mat4.toListRowMajor2d rootTransform

    , "origin-x" Yaml..= case ox of
        Weld.OriginXLeft   -> Yaml.String "left"
        Weld.OriginXMiddle -> Yaml.String "middle"
        Weld.OriginXMean   -> Yaml.String "mean"
        Weld.OriginXRight  -> Yaml.String "right"
        _                  -> error "please don't"

    , "origin-y" Yaml..= case oy of
        Weld.OriginYTop    -> Yaml.String "top"
        Weld.OriginYMiddle -> Yaml.String "middle"
        Weld.OriginYMean   -> Yaml.String "mean"
        Weld.OriginYBottom -> Yaml.String "bottom"
        _                  -> error "please don't"

    , "origin-z" Yaml..= case oz of
        Weld.OriginZNear   -> Yaml.String "near"
        Weld.OriginZMiddle -> Yaml.String "middle"
        Weld.OriginZMean   -> Yaml.String "mean"
        Weld.OriginZFar    -> Yaml.String "far"
        _                  -> error "please don't"

    , "origin" Yaml..= withVec3 adjustOrigin (,,)
    ]
    where
      (ox, oy, oz) = measuredOrigin

loadModels
  :: FilePath
  -> StageRIO env (Resource.ReleaseKey, Files, Collection)
loadModels modelsDir = do
  context <- ask
  (poolsKey, pools) <- CommandBuffer.allocatePools context

  contents <- fmap List.sort $ getDirectoryContents modelsDir

  found <- for contents \path -> do
    let fullPath = modelsDir </> path
    isFile <- doesFileExist fullPath
    if isFile then
      case splitExtensions path of
        (name, ext)
          | ext `elem` [".glb", ".glb.zst", ".gltf", ".gltf.zst"] ->
              pure $ Just (name, fullPath)
        _ ->
          pure Nothing
    else
      pure Nothing

  let collected = Collection.enumerate $ catMaybes found

  let
    filesIds = Map.fromList do
      (ix, (name, _path)) <- collected
      pure (name, ix)

  loaded <- for collected \(_ix, (_name, path)) -> do
    let p4s = path -<.> ".p4s"
    settings@Settings{..} <- fmap (fromMaybe settingsMidBottom) $
      loadSettings p4s

    let p4m = path -<.> ".p4m"
    _processed <- doesFileExist p4m
    ---------------

    let optionsMaterialOffset = 0 -- TODO: remap materials
    (gltf, meshPrimitives) <- Gltf.loadMeshPrimitives True True path
    sceneTreeOriginal <- Gltf.unfoldSceneM optionsMaterialOffset rootTransform gltf meshPrimitives

    let
      originalMeasurements = Weld.measureTree sceneTreeOriginal

      origins = case measuredOrigin of
        (ox, oy, oz) -> [ox, oy, oz]
      originV = Weld.originV originalMeasurements origins + adjustOrigin
      sceneTreeFixed = fmap (Weld.adjustPositions originV) sceneTreeOriginal

      fixedMeasurements = Weld.measureTree sceneTreeFixed

    -- unless processed do
    let
      byBlendMode = Weld.partitionNodes $
        Weld.processSceneNodes
          (Weld.processMeshNode Weld.modelColoredLit Weld.ignoreMaterial)
          sceneTreeFixed

    -- traverse_ (traceShowM . fst) meshPrimitives
    -- error "halt"

    let
      nodePartitions = fmap (map snd . toList) byBlendMode
      nodesBadRanges = fold nodePartitions
      nodes = Storable.fromList $
        zipWith Mesh.adjustRange
          nodesBadRanges
          (List.scanl' (+) 0 $ map (Model.irIndexCount . Mesh.getRange) nodesBadRanges)

    let
      stuffByBlendMode = fmap (Gltf.mergeStuffLike . fmap fst) byBlendMode
      (positions, indices, attrs) = Gltf.mergeStuffLike stuffByBlendMode

    ---------------

    let
      countIndices (_pos, npIndices, _attrs) =
        fromIntegral $ Vector.length npIndices

      meta =
        Weld.sceneMeta
          fixedMeasurements
          (fmap countIndices stuffByBlendMode)
          nodePartitions

      -- pure ()
      -- Weld.encode
      --   p4m
      --   True -- Validate
      --   positions
      --   indices
      --   attrs
      --   nodes
      --   meta

    -- (key, (meta, nodes, _mesh)) <- Mesh.loadIndexed pools p4m
    ---------------

    mesh <- Model.createStaged
      context
      pools
      (Storable.convert positions)
      (Storable.convert attrs)
      (Storable.convert indices)

    key <- Resource.register $
      Model.destroyIndexed context mesh

    pure
      ( key
      , Object
          path
          settings
          gltf
          sceneTreeFixed
          meta
          nodes
          mesh
      )

  let (keys, objects) = List.unzip loaded

  key <- Resource.register $
    traverse_ Resource.release keys

  Resource.release poolsKey

  pure
    ( key
    , filesIds
    , Vector.fromList objects
    )

loadSettings :: HasLogFunc env => FilePath -> RIO env (Maybe Settings)
loadSettings p4s = do
  hasSettings <- doesFileExist p4s
  if hasSettings then
    liftIO (Yaml.decodeFileWithWarnings p4s) >>= \case
      Left err -> do
        logError $ mconcat
          [ "YAML error in settings file ", displayShow p4s, ":\n"
          , fromString $ Yaml.prettyPrintParseException err
          ]
        pure Nothing
      Right (warns, res) -> do
        traverse_ (logWarn . displayShow) warns
        pure res
  else
    pure Nothing

storeSettings :: HasLogFunc env => FilePath -> Settings -> RIO env ()
storeSettings p4s settings = do
  logDebug $ "Storing settings at " <> fromString p4s
  liftIO $ Yaml.encodeFile p4s settings

settingsFile :: Object -> FilePath
settingsFile Object{source} = source -<.> ".p4s"
