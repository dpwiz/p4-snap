module Global.Render
  ( Stage
  , Frame
  , RenderPasses
  , ScreenPasses(..)
  , Pipelines(..)
  , allocatePipelines
  , getSceneLayout
  , getSunLayout
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import RIO.Vector.Partial as Vector (headM)
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (HasSwapchain(getMultisample), RenderPass(..))
-- import Render.Debug.Pipeline qualified as Debug
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.DescSets.Sun (Sun)
import Render.DescSets.Sun qualified as Sun
-- import Render.Font.EvanwSdf.Pipeline qualified as EvanwSdf
import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Render.Lit.Colored.Pipeline qualified as LitColored
-- import Render.Lit.Textured.Pipeline qualified as LitTextured
import Render.Samplers qualified as Samplers
import Render.ShadowMap.Pipeline qualified as ShadowPipe
import Render.ShadowMap.RenderPass (ShadowMap)
import Render.ShadowMap.RenderPass qualified as ShadowPass
-- import Render.Skybox.Pipeline qualified as Skybox
import Render.Unlit.Colored.Pipeline qualified as UnlitColored
import Render.Unlit.Textured.Pipeline qualified as UnlitTextured

type Stage = Engine.Stage RenderPasses Pipelines
type Frame = Engine.Frame RenderPasses Pipelines

type RenderPasses = ScreenPasses

data ScreenPasses = ScreenPasses
  { spForwardMsaa :: ForwardMsaa
  , spShadowPass  :: ShadowMap
  }

-- TODO: BufferPasses, like compute & shadow-mapping

pattern SHADOWMAP_SIZE :: Word32
-- pattern SHADOWMAP_SIZE = 2048 -- XXX: minimal size to for sun type directional light.
pattern SHADOWMAP_SIZE = 4096 -- XXX: should be okay, since the shadows are only updated when needed.
-- pattern SHADOWMAP_SIZE = 8192

pattern SHADOWMAP_BIAS :: (Float, Float)
pattern SHADOWMAP_BIAS = (2.0, 2.5) -- XXX: ok for 2048, 4096
-- pattern SHADOWMAP_BIAS = (4.0, 5) -- XXX: for 8192

instance RenderPass ScreenPasses where
  allocateRenderpass_ context = do
    spForwardMsaa <- ForwardMsaa.allocateMsaa context

    let numLayers = 1 -- XXX: no-cascade sun
    spShadowPass <- ShadowPass.allocate context SHADOWMAP_SIZE numLayers

    pure ScreenPasses{..}

  updateRenderpass context ScreenPasses{..} = ScreenPasses
    <$> ForwardMsaa.updateMsaa context spForwardMsaa
    <*> pure spShadowPass -- XXX: not a screen pass

  refcountRenderpass ScreenPasses{..} = do
    refcountRenderpass spForwardMsaa
    refcountRenderpass spShadowPass

data Pipelines = Pipelines
  -- { pEvanwSdf :: EvanwSdf.Pipeline
  -- , pSkybox   :: Skybox.Pipeline
  -- , pSkySun   :: SkySun.Pipeline
  -- , pDebug    :: Debug.Pipeline

  { pLitColored       :: LitColored.Pipeline
  -- , pLitColoredBlend  :: LitColored.Pipeline
  -- , pLitTextured      :: LitTextured.Pipeline
  -- , pLitTexturedBlend :: LitTextured.Pipeline

  , pUnlitColored        :: UnlitColored.Pipeline
  , pUnlitColoredNoDepth :: UnlitColored.Pipeline
  , pUnlitTextured       :: UnlitTextured.Pipeline
  , pUnlitTexturedBlend  :: UnlitTextured.Pipeline
  , pWireframe           :: UnlitColored.Pipeline
  , pWireframeNoDepth    :: UnlitColored.Pipeline

  , pShadowCast :: ShadowPipe.Pipeline
  }

allocatePipelines
  :: HasSwapchain swapchain
  => swapchain
  -> ScreenPasses
  -> ResourceT (StageRIO st) Pipelines
allocatePipelines swapchain ScreenPasses{..} = do
  (_, samplers) <- Samplers.allocate swapchain
  let msaa = getMultisample swapchain
  let sceneBinds = Scene.set0 samplers (Left 0) (Left 0) 1
  -- let envBinds = SkySun.set1

  -- pDebug    <- Debug.allocate msaa sceneBinds spForwardMsaa
  -- pEvanwSdf <- EvanwSdf.allocate msaa sceneBinds spForwardMsaa
  -- pSkybox   <- Skybox.allocate msaa sceneBinds spForwardMsaa
  -- pSkySun <- SkySun.allocate msaa sceneBinds envBinds spForwardMsaa

  pLitColored       <- LitColored.allocate msaa sceneBinds spForwardMsaa
  -- pLitColoredBlend  <- LitColored.allocateBlend msaa sceneBinds spForwardMsaa
  -- pLitTextured      <- LitTextured.allocate msaa sceneBinds spForwardMsaa
  -- pLitTexturedBlend <- LitTextured.allocateBlend msaa sceneBinds spForwardMsaa

  pUnlitColored        <- UnlitColored.allocate True msaa sceneBinds spForwardMsaa
  pUnlitColoredNoDepth <- UnlitColored.allocate False msaa sceneBinds spForwardMsaa
  pUnlitTextured       <- UnlitTextured.allocate msaa sceneBinds spForwardMsaa
  pUnlitTexturedBlend  <- UnlitTextured.allocateBlend msaa sceneBinds spForwardMsaa
  pWireframe           <- UnlitColored.allocateWireframe True msaa sceneBinds spForwardMsaa
  pWireframeNoDepth    <- UnlitColored.allocateWireframe False msaa sceneBinds spForwardMsaa

  let sunBinds = Sun.set0
  pShadowCast <- ShadowPipe.allocate sunBinds spShadowPass ShadowPipe.defaults
    { ShadowPipe.cull      = Vk.CULL_MODE_BACK_BIT
    , ShadowPipe.depthBias = Just SHADOWMAP_BIAS
    }

  ImGui.allocate swapchain spForwardMsaa 0

  pure Pipelines{..}

getSceneLayout :: Pipelines -> Tagged '[Scene] Vk.DescriptorSetLayout
getSceneLayout Pipelines{pWireframe} =
  case Vector.headM (unTagged $ Pipeline.pDescLayouts pWireframe) of
    Nothing ->
      error "pWireframe has at least set0 in layout"
    Just set0layout ->
      Tagged set0layout

getSunLayout :: Pipelines -> Tagged '[Sun] Vk.DescriptorSetLayout
getSunLayout Pipelines{pShadowCast} =
  case Vector.headM (unTagged $ Pipeline.pDescLayouts pShadowCast) of
    Nothing ->
      error "pShadowCast has at least set0 in layout"
    Just set0layout ->
      Tagged set0layout
