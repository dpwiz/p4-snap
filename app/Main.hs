module Main (main) where

import RIO

import Engine.App (engineMain)

import Stage.Main.Setup (Options(..), stackStage)

main :: IO ()
main = engineMain $ stackStage Options
  { resourcePath = "resources.in/kenney_space-kit"
  }
